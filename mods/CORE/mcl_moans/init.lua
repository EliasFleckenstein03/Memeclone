mcl_moans = {}

function mcl_moans.moan(spec)
	spec.gain = (spec.gain or 1.0) * 10.0
	minetest.sound_play("mcl_moan", spec)
end
